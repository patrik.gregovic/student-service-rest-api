package com.pg.studentservice.controller;

import com.pg.studentservice.model.Course;
import com.pg.studentservice.model.Student;
import com.pg.studentservice.service.StudentService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/students/")
    public List<Student> retrieveAllStudents(){
        return studentService.retrieveAllStudents();
    }

    @GetMapping("/students/{studentId}/courses")
    public List<Course> retrieveCoursesForStudent(@PathVariable String studentId) {
        return studentService.retrieveCourses(studentId);
    }

    @GetMapping("/students/{studentId}/courses/{courseId}")
    public Course retrieveDetailsForCourse(@PathVariable String studentId, @PathVariable String courseId){
        return studentService.retrieveCourse(studentId, courseId);
    }

    @PostMapping("/students/{studentId}/courses")
    public ResponseEntity<Void> registerStudentForCourse(@PathVariable String studentId, @RequestBody Course newCourse){
        Course course = studentService.addCourse(studentId, newCourse);

        if (course == null)
            return ResponseEntity.noContent().build();

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(course.getId()).toUri();

        return ResponseEntity.created(location).build();
    }


    /*
     * TODO: I think RequestBody fetches an entire object and not individiual variables,
     *  thus i would have to probably fetch one object and then fetch each of the attributes
     *  from that object and use that to create the student object with which I can then perform the post request with
     */
    @PostMapping("/students/")
    public ResponseEntity<Void> addStudent(@RequestBody String id, @RequestBody String name, @RequestBody String description, @RequestBody List<Course> courses){
        Student student = studentService.addStudent(id, name, description, courses);

        if (student == null) {
            return ResponseEntity.noContent().build();
        }

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(student.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

}