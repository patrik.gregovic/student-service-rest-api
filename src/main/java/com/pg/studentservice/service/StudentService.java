package com.pg.studentservice.service;

import com.pg.studentservice.model.Course;
import com.pg.studentservice.model.Student;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class   StudentService {

    private static List<Student> students = new ArrayList<>();

    static {
        //initializes data
        Course course1 = new Course("Course1", "Math", "Intro to math",
                Arrays.asList(
                        "First Math Chapter",
                        "Second Math Chapter",
                        "Third Math Chapter",
                        "Fourth Math Chapter"
                ));
        Course course2 = new Course("Course2", "Physics", "Intro to physics",
                Arrays.asList(
                        "First Physics Chapter",
                        "Second Physics Chapter",
                        "Third Physics Chapter",
                        "Fourth Physics Chapter"
                ));
        Course course3 = new Course("Course3", "Biology", "Intro to biology",
                Arrays.asList(
                        "First Biology Chapter",
                        "Second Biology Chapter",
                        "Third Biology Chapter",
                        "Fourth Biology Chapter"
                ));
        Course course4 = new Course("Course4", "Computing", "Intro to computing",
                Arrays.asList(
                        "First Computing Chapter",
                        "Second Computing Chapter",
                        "Third Computing Chapter",
                        "Fourth Computing Chapter"
                ));
        Student student1 = new Student("Patrik Gregovic", "Mathematical Cumputing student", new ArrayList<>(Arrays.asList(course1,course3,course4)));
        Student student2 = new Student( "Gretrik Patrović", "Computational Physics student", new ArrayList<>(Arrays.asList(course2,course3,course4)));
        Student student3 = new Student("Pagov Gretović", "Biology student", new ArrayList<>(Arrays.asList(course3,course4)));

        students.add(student1);
        students.add(student2);
        students.add(student3);

    }

    public static List<Student> retrieveAllStudents() {
        return students;
    }

    public static Student retrieveStudent(String studentId) {
        for (Student student : students ) {
            if (student.getId().equals(studentId))
                return student;
        }
        return null;
    }

    public List<Course> retrieveCourses(String studentId){
        Student student = retrieveStudent(studentId);

        if (student == null) return null;


        return student.getCourses();
    }

    public Course retrieveCourse( String studentId, String courseId){
        Student student = retrieveStudent(studentId);

        if (student == null) return null;

        for (Course course : student.getCourses()){
            if (course.getId().equals(courseId))
                return course;
        }

        return null;
    }

    private SecureRandom random = new SecureRandom();

    public Course addCourse(String studentId, Course course){
        Student student = retrieveStudent(studentId);

        if (student == null) return null;

        String randomId = new BigInteger(130,random).toString(32);
        course.setId(randomId);

        student.getCourses().add(course);

        return course;
    }

    public Student addStudent(String name, String description, List<Course> courses ){
        Student student = new Student(name, description, courses);

        if (student == null) return null;

        students.add(student);

        return student;
    }

    public Student addStudent(String id, String name, String description, List<Course> courses ){
        Student student = new Student(id, name, description, courses);

        if (student == null) return null;

        students.add(student);

        return student;
    }


}
