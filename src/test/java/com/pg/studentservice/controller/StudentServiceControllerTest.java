package com.pg.studentservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
//import com.pg.studentservice.AbstractTest;
import com.pg.studentservice.model.Course;
import com.pg.studentservice.model.Student;
//import com.pg.studentservice.service.StudentService;
import com.pg.studentservice.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(value = StudentController.class)
public class StudentServiceControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private StudentService studentService;

    @Test
    public void retrieveAllStudents() throws Exception {
        String uri = "/students/";

        //create my request
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE);
        //send request and get reply
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        //save response to separate variable
        MockHttpServletResponse response = mvcResult.getResponse();

        System.out.println(response);

        int status = response.getStatus();
        assertEquals(200, status);
        String content = response.getContentAsString();
        Student[] studentlist = new ObjectMapper().readValue(content, Student[].class);
        assertTrue(studentlist.length > 0);
    }

    @Test
    public void addStudent() throws Exception {
        String uri = "/students/";
        Student student = new Student();
        student.setName("Test McUnit");
        student.setDescription("Student created by unit test");
        student.setCourses(
                new ArrayList<>(Arrays.asList(
                        new Course(
                                "TestCourse1",
                                "Test Course",
                                "Course made by UnitTest",
                                Arrays.asList(
                                        "First Test Chapter",
                                        "Second Test Chapter",
                                        "Third Test Chapter",
                                        "Fourth Test Chapter"
                                )
                        )
                ))
        );
        String inputJson = new ObjectMapper().writeValueAsString(student);
        System.out.println(inputJson);
        //create my request
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri)
                .characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson);
        //execute request and get reply
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
        //save response to separate variable
        MockHttpServletResponse response = mvcResult.getResponse();

        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        String content = response.getContentAsString();
        assertEquals(content, "Student is created successfully");
    }
}